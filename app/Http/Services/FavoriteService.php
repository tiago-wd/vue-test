<?php

namespace App\Http\Services;

use App\User;
use Carbon\Carbon;

class FavoriteService
{
    public function getLastFavoritePhotos()
    {
        $users = User::all();
        return $users->map(function($user) {
            return $user->lastFavorite()->first();
        });
    }
    
    public function getUsersMostFavorited()
    {
        $users = User::all();
        $data = $users->map(function($user) {
            $user->countFavorites = $user->favorites
                                        ->where('created_at', '>=', Carbon::now()->startOfWeek())
                                        ->where('created_at', '<=', Carbon::now()->endOfWeek())
                                        ->count();
            return $user;
        });
        return $data->sortByDesc('countFavorites')->values();
    }
}
