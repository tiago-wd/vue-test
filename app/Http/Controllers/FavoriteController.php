<?php

namespace App\Http\Controllers;

use App\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function favorite(Request $request)
    {
        try {
            $params = $request->get('photo');
            $params['photo_id'] = $request->get('photo')['id'];
            return Auth::user()->favorites()->create($params);
        } catch (Exception $e) { 
            return false;
        }
    }

    public function unFavorite(Request $request)
    {
        try {
            return Favorite::where('user_id', Auth::user()->id)
                            ->where('photo_id', $request->get('photo')['id'])
                            ->delete();
        } catch (Exception $e) { 
            return false;
        }
    }

    public function isFavorite(Request $request)
    {
        return response()->json(Favorite::where('user_id', Auth::user()->id)
                        ->where('photo_id', $request->get('photo')['id'])
                        ->exists());
    }
}
