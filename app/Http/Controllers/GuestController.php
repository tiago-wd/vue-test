<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Services\FavoriteService;

class GuestController extends Controller
{
    private $favoriteService;

    public function __construct(FavoriteService $favoriteService)
    {
        $this->middleware('guest');
        $this->favoriteService = $favoriteService;
    }

    public function index()
    {
        if(Carbon::now()->isWeekday()) {
            $data = $this->favoriteService->getLastFavoritePhotos();
        } else {
            $data = $this->favoriteService->getUsersMostFavorited();
        }

        return view('guest', compact('data'));
    }

}
