@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <guest :data="{{$data}}"></guest>               
        </div>
    </div>
</div>

@endsection